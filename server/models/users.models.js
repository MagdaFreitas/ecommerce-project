const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    email:{ type:String, unique:true, required:true },
    password:{ type:String, required:true },
    admin:{ type:Boolean, required:true },
    name:String,
    address:String,
    postCode:Number,
    city:String
});
module.exports = mongoose.model('users', userSchema);




