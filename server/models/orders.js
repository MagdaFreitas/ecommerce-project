const mongoose = require ('mongoose');

const ordersSchema = new mongoose.Schema({

  cart:Array, 
  email:String,
  totalPrice:Number, 
  date: {type:Date, default:Date.now}
  
  
});

module.exports = mongoose.model('orders', ordersSchema);
