const mongoose = require ('mongoose');

const productSchema = new mongoose.Schema({
  category:String, 
  name:String, 
  price:Number, 
  description:String,
  onSale:Boolean,
  bestSeller:Boolean,
  showCase:Boolean,
  image:Object,
  stock:Number
});

const Products = mongoose.model('products', productSchema);

module.exports = Products;
