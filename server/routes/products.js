const router = require('express').Router();
const controller = require ('../controllers/products');

router.get('/', controller.find);

router.get('/find/:id', controller.findOneId);

router.post('/new', controller.create);

router.post('/delete', controller.delete);

router.post('/update',controller.update);

router.get('/bestsellers', controller.findBestSellers);

router.get('/showCase', controller.findShowCase);

router.get('/onsale', controller.findOnSale);

router.get('/categories/:category', controller.findCategory);

router.get('/:name', controller.findOne);

module.exports = router;
