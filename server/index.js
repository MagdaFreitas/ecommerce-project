const express = require('express');
const app  = require('express')();
const port = process.env.port || 3030;
const path = require('path');

require('dotenv').config();
// =============== BODY PARSER SETTINGS =====================
const bodyParser= require('body-parser');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// =============== DATABASE CONNECTION =====================
const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
async function connecting(){
    try {
        await mongoose.connect(process.env.ATLAS_URL, { useUnifiedTopology: true , useNewUrlParser: true })
        console.log('Connected to the DB')
    } catch ( error ) {
        console.log('ERROR: Seems like your DB is not running, please start it up !!!');
    }
}
connecting()    
//================ CORS ================================
const cors = require('cors');

app.use(cors());

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");

	next();
});
// =============== ROUTES ==============================
app.use('/users', require('./routes/users.routes'));
app.use('/products', require('./routes/products.js'));
app.use("/payment", require("./routes/payment.js"));
app.use('/emails', require('./routes/emails.js'))

app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, '../client/build')));
app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, '../client/build', 'index.html'));
});

// =============== START SERVER =====================
app.listen(port, () => 
    console.log(`server listening on port ${port}`
));