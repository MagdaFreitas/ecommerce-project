const Products = require ('../models/products');
const cloudinary = require('cloudinary')

cloudinary.config({ 
  cloud_name: process.env.CLOUD_NAME, 
  api_key:    process.env.API_KEY, 
  api_secret: process.env.API_SECRET
});

class ProductsController {

async find (req, res){
  try {
    const myProduct = await Products.find({});
    res.send({myProduct});
  }
  catch (error) {
    res.send({error});
  };
}

async findOne (req, res) {
  let { name } = req.params;
  try {
    const myProduct = await Products.find ({ name:name });
    res.send({myProduct});
  }
  catch (error) {
    res.send({error});
  };
}

async findOneId (req, res) {
  let { id } = req.params;
  try {
    const myProduct = await Products.find ({ _id:id });
    res.send({myProduct});
  }
  catch (error) {
    res.send({error});
  };
}

async findBestSellers (req, res) {
  try {
    const myProduct = await Products.find ({ bestSeller:true });
    res.send({myProduct});
  }
  catch (error) {
    res.send({error});
  };
}

async findOnSale (req, res) {
  try {
    const myProduct = await Products.find ({ onSale:true });
    res.send({myProduct});
  }
  catch (error) {
    res.send({error});
  };
}

async findShowCase (req, res) {
  try {
    const myProduct = await Products.find ({ showCase:true });
    res.send({myProduct});
  }
  catch (error) {
    res.send({error});
  };
}

async findCategory (req, res) {

  let {category} = req.params;

  try {
    const myProduct = await Products.find ({ category:category });
    res.send({myProduct});
  }
  catch (error) {
    res.send({error});
  };
}

async create (req, res) {
  let { category, name, price, description, bestSeller, onSale,showCase,image, stock } = req.body;
  if (!name || !category || !price || !description || !onSale || !bestSeller || !showCase || !image || !stock) return res.json({ok:false, message:'All fields are required' });
  try {
    const myProduct = await Products.findOne({name:name});
    if (myProduct) return res.json({ok:false,message:'Product already exists'})
    const newProduct = await Products.create ({ 
            category:category,
            name:name,
            price:price,
            description:description,
            bestSeller: bestSeller,
            onSale:onSale,
            showCase:showCase,
            image:image,
            stock:stock
        
    });
    await Products.create (newProduct)
    res.json({ok:true, message: 'Product created'})
    }
  catch (error) {
    res.send({error});
  };
}

async delete (req,res){
  let { name } = req.body;
  if (!name) return res.json({ok:false, message:'Product name required' });
  try {
    const myProduct = await Products.findOne({name:name})
    const removed = await Products.deleteOne({name})
    await cloudinary.v2.api.delete_resources([ myProduct.image.public_id ]);
    res.json({ok:true, message: 'Product deleted'})
  }
  catch (error) {
    res.send({error});
  };
}

async update (req,res){
  let { name, newName, category, price, description,bestSeller,onSale,showCase,image,stock } = req.body;
  if (!name || !category || !price || !description || !onSale || !bestSeller || !showCase || !image || !stock) return res.json({ok:false, message:'All fields are required' });
  try {
    const updated = await Products.updateOne ({ name }, { 
      name:newName,
    category:category,
    price:price,
    description:description,
    bestSeller: bestSeller,
    onSale:onSale,
    showCase:showCase,
    image:image,
    stock:stock});
    
    await Products.create (updated)
    res.json({ok:true, message: 'Product updated'})
    res.send({updated});

  }
  catch (error) {
    res.send({error});
  };
}

};
module.exports = new ProductsController();