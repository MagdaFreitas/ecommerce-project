const User       = require('../models/users.models'); 
const argon2     = require('argon2');//https://github.com/ranisalt/node-argon2/wiki/Options
const jwt        = require('jsonwebtoken');
const validator  = require('validator');
const jwt_secret = process.env.JWT_SECRET
const Orders = require ('../models/orders');


const register = async (req,res) => {
	const { email , password , password2,name,address,postCode,city } = req.body;
	if(!email || !password || !password2 || !name || !address || !postCode || !city ) return res.json({ok:false,message:'All fields are required'});
    if( password !== password2 ) return res.json({ok:false,message:'Passwords must match'});
    if( !validator.isEmail(email) ) return res.json({ok:false,message:'Please provide a valid email'})
    try{
    	const user = await User.findOne({ email })
    	if( user ) return res.json({ok:false,message:'Email already in use'});
    	const hash = await argon2.hash(password)
        console.log('hash ==>' , hash)
        const newUser = {
        	email:email,
            password : hash,
            admin: false,
            name:name,
            address:address,
            postCode:postCode,
            city:city          
        }
        await User.create(newUser)
        res.json({ok:true,message:'Successful registration!'})
    }catch( error ){
        res.json({ok:false,error})
    }
}

const login = async (req,res) => {
    const { email , password } = req.body;
    if( !email || !password ) res.json({ok:false,message:'All fields are required'});
    if( !validator.isEmail(email) ) return res.json({ok:false,message:'Please provide a valid email'});
	try{
    	const user = await User.findOne({ email });
    	if( !user ) return res.json({ok:false,message:'Please provide a valid email'});
        const match = await argon2.verify(user.password, password);
        if(match) {
           const token = jwt.sign(user.toJSON(), jwt_secret ,{ expiresIn:100080 });//{expiresIn:'365d'}
           res.json({ok:true,message:'Welcome back!',token,email}) 
        }else return res.json({ok:false,message:'Invalid password'})
    }catch( error ){
    	 res.json({ok:false,error})
    }
}

const verify_token = (req,res) => {
  console.log(req.headers.authorization)
  const token = req.headers.authorization;
       jwt.verify(token, jwt_secret, (err,succ) => {
             err ? res.json({ok:false,message:'Something went wrong'}) : res.json({ok:true,succ})
       });      
}

const findUser = async (req,res) => {
    console.log(req.headers.authorization)
    const token = req.headers.authorization;
        try{
            const decoded = await jwt.verify(token, jwt_secret)
            if(!decoded) {return res.send({ok:false, message:'No user found'})}
            const myUser = await User.findOne({ _id: decoded._id })
            // console.log('product controller findBestseller : ', myProduct)
            res.send({myUser});
        } catch(error) {
            res.json({ok:false, error})
        }
    }

const findOrders = async (req, res) =>{
    let { email } = req.params;
     try{
        const myProduct = await Orders.find({ email: email })
        res.send({myProduct});
        }
        catch(error){
        res.send({error});
        };
        }


module.exports = { register , login , verify_token, findUser, findOrders }
