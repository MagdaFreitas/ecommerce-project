import React from 'react';
import SingleProduct from '../renderers/SingleProduct.js'

  let ProductList = props =>{

    let productItem = props.products.map((item, i) => {
      return <SingleProduct {...props} key={i} itemProduct={item}/>
    })

return <div className='grid'>
  {productItem}
</div>

}

export default ProductList;