import React, {useState, useEffect, useRef} from 'react';



let SingleProduct = (props) => {

  const [cart, setCart] = useState([]);
  const [message, setMessage] = useState('')

  useEffect(() => {
    const timer = setTimeout(() => { setMessage('') }, 1500);
    return () => clearTimeout(timer);
  }, [message]);
  
  let toLocalStorage = () => {

    let tempCart = JSON.parse(localStorage.getItem("cart")) || [];
    let cartBoolean = false;

    tempCart.map((itemProduct, i) => {
    if (itemProduct.id.indexOf(props.itemProduct._id) != -1){cartBoolean = true}});
    
    if (cartBoolean == false) {tempCart.push({ id:props.itemProduct._id, amount: 1 });}

    if (tempCart.length === 0) {tempCart.push({ id:props.itemProduct._id, amount: 1 });}

    localStorage.setItem("cart", JSON.stringify(tempCart));
    
    setMessage('Added to cart')
      
    return console.log('sent')
  };

    return <div className='singleProductContainer'>
      <section className='singleProductContainerSection'>
    <h3 className='singleProductContainerSectionName' onClick={() => props.history.push(`/products/${props.itemProduct.name}`)}>{props.itemProduct.name}</h3>
    <p className='singleProductContainerSectionParagraph'>{props.itemProduct.category}</p>
    <img className='singleProductContainerSectionImage' src={props.itemProduct.image.photo_url}></img>
    <span className='singleProductSpan'><h2 className='singleProductContainerSectionPrice'>{props.itemProduct.price}€</h2>
    <button className='singleProductContainerSectionButton' onClick={toLocalStorage}>Buy</button></span>
    <span className='singleProductContainerSectionTimeOut'>{message}</span>
    </section>
    </div>
  }



export default SingleProduct;