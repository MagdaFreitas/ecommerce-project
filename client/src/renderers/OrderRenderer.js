import React from 'react';
import moment from 'moment';


function OrderRenderer(props) {

let total = 0;
let date = moment(props.order.date).format('YYYY-MM-DD')
let time = moment(props.order.date).format('HH:MM:SS')
return (
  <div className='orderContainer' >
    <h3>Order Id: {props.order._id}</h3> 
<h4>Date: {date}</h4>
<h4>Time: {time}</h4> 
    {
      props.order.cart.map((cartItem,i) => {
        return (
          props.product.map((productItem, i) =>{
            console.log('product item====>', productItem)
            if (cartItem.id === productItem._id){
              total += cartItem.amount * productItem.price;
              return <div className='orderList'>
              <h4>{productItem.name}</h4>
              <h4>{productItem.price}€</h4>
              <h4>Qty:{cartItem.amount}</h4>
                </div>
            }
          })
        )
      })
    }
    <h1>Order total:  {total}€</h1>
  </div>
)
}

export default OrderRenderer;