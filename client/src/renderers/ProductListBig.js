import React from 'react';
import SingleProductBig from '../renderers/SingleProductBig.js'

  let ProductList = props =>{

    let productItem = props.products.map((item, i) => {
      return <SingleProductBig {...props} key={i} itemProduct={item}/>
    })

return <div>
  {productItem}
</div>

}

export default ProductList;