import React, { useEffect } from "react";
import axios from "axios";
import {URL} from '../config.js'

const PaymentSuccess = props => {
  useEffect(() => {
    getSessionData();
  }, []);
  const getSessionData = async () => {
    try {
      const cart = JSON.parse(localStorage.getItem('cart'));
      const sessionId = JSON.parse(localStorage.getItem("sessionId"));

      const response = await axios.post(
        `${URL}/payment/checkout-session?sessionId=${sessionId}`, {cart});

      const email = await axios.post(`${URL}/emails/send_email`, {name: `Super Admin`, email: `${response.data.customer.email}`, subject: `Receipt: ${response.data.customer.id}`, message: `Your stuff will be sent, thanks for paying.`})

      localStorage.removeItem("sessionId");
      localStorage.removeItem('cart')
      console.log("== response ==>", response);
      //if you need the products list in this page, you can find them in : response.data.session.display_items
    } catch (error) {
      //handle the error here, in case of network error
    }
  };
  return (
    <div className="message_container">
      <div style={{ border: "2px solid  #35BFDE" }} className="message_box">
        <div className="message_box_left">
          <img
            alt="smile_icon"
            className="image"
            src={
              "https://res.cloudinary.com/estefanodi2009/image/upload/v1578495645/images/smile.png"
            }
          />
        </div>
        <div style={{ color: "#35BFDE" }} className="message_box_right">
          Payment successful, thank you for your purchase! A confirmation e-mail has been sent to you.
        </div>
      </div>
    </div>
  );
};

export default PaymentSuccess;
