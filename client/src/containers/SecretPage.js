import React, {useState} from 'react'
import Axios from 'axios'
import UploadImages from '../UploadImages'
import {URL} from '../config.js'

const SecretPage = (props) => {

	const [form, setValues] = useState({
		name: '',
		newName:'',
		category:'',
		price:0,
		description:''
	})

	const [imageUpload, setImageUpload] = useState()

	const [addMessage, setAddMessage] = useState('')
	const [deleteMessage, setDeleteMessage] = useState('')
	const [updateMessage, setUpdateMessage] = useState('')

	const handleChange = e => {
		setValues({...form,[e.target.name]:e.target.value})
	}


	const handleAddProduct = async (e) => {
		e.preventDefault()
			if (!imageUpload) {
				setAddMessage('Image required')
				return null;
			}
		try {
			const response = await Axios.post(`${URL}/products/new`, {
				name:form.name,
				category:form.category,
				price:form.price,
				description:form.description,
				onSale:form.onSale,
				bestSeller:form.bestSeller,
				showCase:form.showCase,
				image:imageUpload,
				stock:form.stock
			})
			setAddMessage(response.data.message)
			setImageUpload()
		}
		catch (error){
			console.log(error)
		}
	document.forms['addForm'].reset();
	} 

	const handleDeleteProduct = async (e) => {
		e.preventDefault()
		try {
			const response = await Axios.post(`${URL}/products/delete`,{
				name:form.name
			})
			setDeleteMessage(response.data.message)
		}
		catch (error){
			console.log(error)
		}
	document.forms['deleteForm'].reset();
	} 


	const handleUpdate = async (e) => {
		e.preventDefault()
		try {
			const response = await Axios.post(`${URL}/products/update`,{
				name:form.name,
				newName:form.newName,
				category:form.category,
				price:form.price,
				description:form.description,
				onSale:form.onSale,
				bestSeller:form.bestSeller,
				showCase:form.showCase,
				image:form.url,
				stock:form.stock
			})
			setUpdateMessage(response.data.message)
		}
		catch (error){
			console.log(error)
		}
	document.forms['updateForm'].reset();
	} 

	return <div className='secret_page'>
	<div>
	    
<div className='forms'>
  <form id='addForm' onSubmit={handleAddProduct}
	onChange={handleChange}
	>
		 <h3>Add product to DB</h3>
  <label>
    Product Name:   
  </label>
	<input type="text" name="name" /><br/>
  

  <label>
    Category:   
  </label>
	<input type="text" name="category" /><br/>


  <label>
    Price:   
  </label>
	 <input type= 'number' name="price" /><br/>
 

  <label>
    Description:   
  </label>
	<input type="text" name="description" /><br/>

	<label>
    On Sale:   
  </label>
	<input type="boolean" name="onSale" /><br/>

	<label>
    Best seller:   
  </label>
	<input type="boolean" name="bestSeller" /><br/>

	<label>
    Show Case:   
  </label>
	<input type="boolean" name="showCase" /><br/>

	<label>
    Stock:   
  </label>
	<input type="number" name="stock" /><br/>

	<input type='submit' value='Create'/>
	<h3>{addMessage}</h3>
</form>
<div className='imageUp'><h2>Please upload an image:</h2> <UploadImages setImageUpload={setImageUpload}/> </div>
</div>



</div>

<div>
<form onSubmit={handleDeleteProduct}
onChange={handleChange} id='deleteForm' className='forms'>
	<h3>Delete product from DB</h3>
<label>
Product Name:   
</label>
<input type="text" name="name" />
<input type="submit" value="Delete" />
<h3>{deleteMessage}</h3>
</form>

</div>

<div>
 <form onSubmit={handleUpdate}
          onChange={handleChange} id='updateForm' className='forms'>
						<h3>Update a product in DB</h3>
      <label>
        Name:   
      </label>
      <input type="text" name="name" /><br/>
			<label>
       newName:   
      </label>
      <input type="text" name="newName" /><br/>
      <label>
        Category:   
      </label><input type="text" name="category" /><br/>
      <label>
       Price:   
      </label><input type= 'number' name="price" /><br/>
      <label>
        Description:   
      </label><input type="text" name="description" /><br/>
			<label>
    On Sale:   
  </label>
	<input type="boolean" name="onSale" /><br/>

	<label>
    Best seller:   
  </label>
	<input type="boolean" name="bestSeller" /><br/>

	<label>
    Show Case:   
  </label>
	<input type="boolean" name="showCase" /><br/>

	<input type="text" name="url" /><br/>

	<label>
    Stock:  
  </label>
	<input type="number" name="stock" /><br/>

			<input type='submit' value='Update'/>
			<h3>{updateMessage}</h3>
    </form>

</div>

<div className='secret-page-div-btn'>
	<button className='secret-page-btn' onClick={()=>{
				  props.history.push('/users');
				  props.logout()
				  }}>logout</button>
	       </div>


</div>
}

export default SecretPage










