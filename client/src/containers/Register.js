import React , { useState } from 'react'
import Axios from 'axios' 
import {URL} from '../config.js'

const Register = (props) => {
	const [ form , setValues ] = useState({
		email    : '',
		password : '',
		password2: '',
		name: '',
		address: '',
		postCode: '',
		city: ''
	})
	const [ message , setMessage ] = useState('')
	const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
	}
	const handleSubmit = async (e) => {
		e.preventDefault()
		try{
			const response =  await Axios.post(`${URL}/users/register`,{
	        email    : form.email,
			    password : form.password,
					password2: form.password2,
					name: form.name,
					address: form.address,
					postCode: form.postCode,
					city: form.city
	        })
					setMessage(response.data.message)
				if (response.data.ok === true) {setTimeout(() => props.history.push('/users'),2000)}
		}
		catch( error ){
			console.log(error)
		}

	}
	return <form onSubmit={handleSubmit}
	             onChange={handleChange}
	             className='form_container'>
				<label>Name:</label>
		     <input name="name"/>
				 <label>Address:</label>
		     <input name="address"/>
				 <label>Post Code:</label>
		     <input name="postCode"/>
				 <label>City:</label>
		     <input name="city"/>
	      <label>Email:</label>
		     <input name="email"/>
		     <label>Password:</label>
		     <input name="password"/>
		     <label>Repeat password:</label>
		     <input name="password2"/>
		     <button>Register</button>
		     <div className='message'><h4>{message}</h4></div>
	       </form>
}

export default Register