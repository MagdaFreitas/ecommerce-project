import React, {useState, useEffect} from "react";
import Product from "../components/product";
import axios from "axios";
import { injectStripe } from "react-stripe-elements";
import {URL} from '../config.js'

const Checkout = props => {

  const [ourCart, setOurCart] = useState([]);

  const calculate_total = () => {
    let total = 0;
    ourCart.forEach(ele => (total += ele.quantity * ele.amount));
    return total;
  };

  useEffect(() => {
    let cart = JSON.parse(localStorage.getItem("cart")) || [];
    setOurCart(cart);
    nameSwap()
  }, []);

  let nameSwap = () => {

    let tempCart = [];
    props.location.state.map((itemProduct, i) => {
      // console.log('this is checkOut page item: ', itemProduct)
      let tempProduct = {name:itemProduct.name, images:[itemProduct.image.photo_url], amount:itemProduct.price, quantity:itemProduct.amount}
      tempCart.push(tempProduct)
      // console.log('this is checkOut page temp: ', tempProduct)
    })
 setOurCart(tempCart)
  }

  //=====================================================================================
  //=======================  CREATE CHECKOUT SESSION  ===================================
  //=====================================================================================
  const createCheckoutSession = async () => {
    try {
      const response = await axios.post(
        `${URL}/payment/create-checkout-session`,
        { products:ourCart }
      );
      return response.data.ok
        ? (localStorage.setItem(
            "sessionId",
            JSON.stringify(response.data.sessionId)
          ),
          redirect(response.data.sessionId))
        : props.history.push("/payment/error");
    } catch (error) {
      props.history.push("/payment/error");
    }
  };
  //=====================================================================================
  //=======================  REDIRECT TO STRIPE CHECKOUT  ===============================
  //=====================================================================================
  const redirect = sessionId => {
    props.stripe
      .redirectToCheckout({
        // Make the id field from the Checkout Session creation API response
        // available to this file, so you can provide it as parameter here
        // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
        sessionId: sessionId
      })
      .then(function(result) {
        // If `redirectToCheckout` fails due to a browser or network
        // error, display the localized error message to your customer
        // using `result.error.message`.
      });
  };
  //=====================================================================================
  //=====================================================================================
  //=====================================================================================
  return (
    <div className="checkout_container">
      <h1 className="topCheckout">Checkout - Check products before payment</h1>
      <div className="products_list">
        {ourCart.map((item, idx) => {
          return <Product key={idx} itemProduct= {item} />;
        })}
      </div>
      <div>
        <div className="total">Total : {calculate_total()} €</div>
        <button className="button" onClick={() => createCheckoutSession()}>
          PAY
        </button>
      </div>
    </div>
  );
};

export default injectStripe(Checkout);