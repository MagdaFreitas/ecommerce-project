import React, { useState, useEffect } from "react";
import axios from "axios";
import trash from '../components/trash.png'

const Cart = (props) => {
  const [ourCart, setOurCart] = useState([]);
  const [total, setTotal] = useState(0);
  
  useEffect(() => {
    let cart = JSON.parse(localStorage.getItem("cart")) || [];
    setOurCart(cart);
  }, []);

  let countTotal = () => {
    let count = 0;
    ourCart.map(itemProduct => {
      count += itemProduct.amount * itemProduct.price;
    })
    setTotal(count);
  }

  useEffect(() => {
   fSetTrueCart();
  }, []);


  useEffect(() => {
    countTotal()}, [ourCart])

  let fSetTrueCart = () => {
    let cart = JSON.parse(localStorage.getItem('cart')) || [];
    let cartTrue = [];
    cart.map((singleProduct,i)=> {
      const index = props.products.findIndex((item) => item._id === singleProduct.id);
      if (index === -1) { console.log('something') } else {cartTrue.push({...props.products[index], amount:singleProduct.amount})}
    })
    setOurCart([...cartTrue])
  }

  let increase = i => {
    let tempProduct = ourCart;
    tempProduct[i].amount += 1;
    setOurCart(tempProduct, countTotal());

    let cart = JSON.parse(localStorage.getItem('cart')) || [];

    const index = cart.findIndex((item) => item.id === tempProduct[i]._id); 
    cart[index].amount += 1;
    localStorage.setItem('cart', JSON.stringify(cart))
  }

  let decrease = i => {
    let tempProduct = ourCart;
    tempProduct[i].amount -= 1;

    setOurCart(tempProduct, countTotal());

    let cart = JSON.parse(localStorage.getItem('cart')) || [];
    const index = cart.findIndex((item) => item.id === tempProduct[i]._id); 
    cart[index].amount -= 1;
    localStorage.setItem('cart', JSON.stringify(cart))
  }

  let remove = i => {
  let tempProduct = ourCart;

  let cart = JSON.parse(localStorage.getItem('cart')) || [];
  const index = cart.findIndex((item) => item.id === tempProduct[i]._id); 
  cart.splice(index,1)
  localStorage.setItem('cart',JSON.stringify(cart))

  tempProduct.splice(i,1)
  setOurCart([...tempProduct])
  }

  let cartProducts = ourCart.map((ele, i) => {
    return (
      <div key={i}>
        <h2>Name: {ele.name}</h2>
      
        <img src={ele.image.photo_url} alt='image'/>
        <span className='spanAll'>
        <button className='spanCart' disabled = {ele.amount === 1} onClick={() => decrease(i)}>-</button>
        <h2 className='spanCart'>Qty: {ele.amount}</h2>
        <button className='spanCart' onClick={() => increase(i)}>+</button>
        <img className='icon spanCart' onClick ={() => remove(i)} src={trash}/></span>
        <h2 className='spanCart'>Price: {ele.price}€</h2>
      </div>
    );
  });

  return (
    <div>
      <div className='displayCartProds'>
      {cartProducts}
      </div>
      <h1>Total:{total}€</h1>
      <button disabled = {total === 0} className='checkOutBtn' onClick={() => props.history.push({pathname:'/stripe', state:ourCart})}> Proceed to checkout </button>
    </div>
  );
};
export default Cart;