import React, { useEffect, useState } from "react";
import ProductList from '../renderers/ProductList.js'
import Axios from 'axios'
import {URL} from '../config.js'

const Categories = (props) => {

  const [productState, setProductState] = useState([])

  let getProduct = async () => {
    try {
      let response = await Axios.get(`${URL}/products/categories/${props.match.params.category}`)
      setProductState([...response.data.myProduct])
    } catch (error){
      console.log(error)
    }
  }

useEffect(() => {

getProduct()
}, [props.match.params.category])


  return <div>
    
    <ProductList {...props} products={productState}/>
  </div>
};

export default Categories;