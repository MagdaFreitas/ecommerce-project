import React, {useState, useEffect} from 'react'
import axios from 'axios'
import {URL} from '../config'
import OrderRenderer from '../renderers/OrderRenderer.js'

const UserPage = (props) => {

	const [user, setUser] = useState({})
	const [orders, setOrders] = useState([])

	useEffect(() => {
		getUser();
	}, [])

	let getUser = async () => {
    const token = JSON.parse(localStorage.getItem('token'))
    axios.defaults.headers.common['Authorization'] = token
		const response = await axios.get(`${URL}/users/findUser`)
		console.log('user page response ===>', response)
    setUser(response.data.myUser)
 }

 
 useEffect(() => {
 getOrders()
}, [user])

useEffect(() => {
	console.log('user page.orders> ', orders, 'user page response.props.products> ', props.product )
 }, [orders])

let getOrders = async () => {
	const response = await axios.get(`${URL}/users/orders/${user.email}`)
	console.log('User page get orders response ===>', response)
	setOrders(response.data.myProduct)
}

let orderMapper = (orders) => {
	let renderedOrder = orders.map((order,i) => {
		return <OrderRenderer key={i} {...props} product={props.product} order={order}/>
	})
	return <div>{renderedOrder}</div>
}

	return <div>
		<h2 className='h2Hello'>Welcome back {user.name}!</h2>
	<div className='userPageAccountD'>
	
	<h3>Your details:</h3>
	{user.name}<br/>
	{user.email}<br/>
	{user.address}<br/>
	{user.postCode}<br/>
	{user.city}<br/>
</div>

<div className='userPageOrderD'>
<h1>Your orders:</h1>
{orderMapper(orders)}
</div>

<div className='secret-page-div-btn'>
	<button classname='secret-page-btn' onClick={()=>{
				  props.history.push('/');
				  props.logout()
				  }}>Logout</button>
	       </div>


</div>
}

export default UserPage