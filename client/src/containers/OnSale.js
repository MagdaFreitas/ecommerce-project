import React, { useEffect, useState } from "react";
import ProductList from '../renderers/ProductList.js'
import Axios from 'axios'
import {URL} from '../config.js'

const OnSale = (props) => {
  const [productState, setProductState] = useState([])

useEffect(() => {
  let getProduct = async () => {
    try {
      let response = await Axios.get(`${URL}/products/onsale`)
      setProductState([...response.data.myProduct])
    } catch (error){
      console.log(error)
    }
  }
getProduct()
}, [])

useEffect(() =>{
}, [productState])

  return <div>
    
    <ProductList {...props} products={productState}/>
  </div>
};

export default OnSale;