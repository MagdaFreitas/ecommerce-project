import React, { useEffect, useState } from "react";
import ProductListBig from '../renderers/ProductListBig.js'
import Axios from 'axios'
import {URL} from '../config.js'

const SingleProductPage = (props) => {
  const [productState, setProductState] = useState([])

useEffect(() => {
  let getProduct = async () => {
    try {
      let response = await Axios.get(`${URL}/products/${props.match.params.name}`)
      setProductState([...response.data.myProduct])
    } catch (error){
      console.log(error)
    }
  }
getProduct()
}, [])


  return <div>
    
    <ProductListBig {...props} products={productState}/>
  </div>
};

export default SingleProductPage;