import React , { useState , useEffect } from 'react'
import axios from 'axios' 
import { BrowserRouter as Router , Route , Redirect } from 'react-router-dom'
import Header from './components/header.js';
import Footer from './components/footer.js'
import Login from './containers/Login.js'
import Register from './containers/Register.js'
import SecretPage from './containers/SecretPage.js'
import Navbar from './components/Navbar.js'
import { URL } from './config.js'
import AllProducts from "./containers/AllProducts.js";
import Categories from "./containers/Categories.js"
import BestSellers from "./containers/BestSellers.js";
import ShowCase from "./containers/ShowCase.js";
import OnSale from "./containers/OnSale.js";
import Cart from './containers/Cart.js';
import SingleProductPage from './containers/SingleProductPage.js'
import UserPage from './containers/UserPage.js'

import Stripe from './components/stripe.js'
import PaymentSuccess from './containers/payment_success.js'
import PaymentError from './containers/payment_error.js'

import "./App.css";


function App() {

  const [product, setProduct] = useState([])
  const [isLoggedIn,setIsLoggedIn] = useState(false)
  const [isAdmin, setIsAdmin] = useState(false)
    
  const token = JSON.parse(localStorage.getItem('token'))

  const verify_token = async () => {
   if( token === null )return setIsLoggedIn(false)
     try{
          axios.defaults.headers.common['Authorization'] = token
          const response = await axios.post(`${URL}/users/verify_token`)
          if (response.data.succ.admin) { setIsAdmin(true)} else { setIsAdmin(false)}
          return response.data.ok ? setIsLoggedIn(true) : setIsLoggedIn(false)
     }
     catch(error){
        console.log(error)
     }
  }

  useEffect( () => {
     verify_token()
     getProduct()
  },[])


  const login  = (token) => {
     console.log('token ===>')
     localStorage.setItem('token',JSON.stringify(token)) 
     setIsLoggedIn(true)
  }
  const logout = () => {
     localStorage.removeItem('token');
     setIsLoggedIn(false)
  }
  
  let getProduct = async () => {
   try {
     const response = await axios.get(`${URL}/products`)
     setProduct([...response.data.myProduct])
   } catch (error) {
     console.log(error)
   }
 }

  return (
     <Router>

      <Header/>
        <Navbar isAdmin={isAdmin} isLoggedIn={isLoggedIn}/>
        <Route exact path='/users'   render={ props =>  isLoggedIn ? <Redirect to={'/'}/> : <Login login={login} {...props}/>} />

        <Route exact path='/users/userpage'   render={ props =>  !isLoggedIn ? <Redirect to={'/'}/> : <UserPage  logout={logout} product={product} {...props}/>} />

        <Route exact path='/users/register'   render={ props =>  isLoggedIn ? <Redirect to={'/'}/> : <Register {...props}/>}/>
        <Route exact path='/users/secret-page' render={ props => {
                                            return !isAdmin
                                            ? <Redirect to={'/'}/>
                                            : <SecretPage logout={logout} {...props}/>   
        }}/>
      <Route exact path="/" render = {(props) => <ShowCase {...props}/>} />
      <Route exact path="/All" render = {(props) => <AllProducts {...props}/>} />
      <Route exact path="/Categories/:category" render = {(props) => <Categories {...props}/>} />
      <Route exact path="/BestSellers" render = {(props) => <BestSellers {...props}/>} />
      <Route exact path="/OnSale" render = {(props) => <OnSale {...props}/>} />
      <Route exact path="/products/:name" render = {(props) => <SingleProductPage {...props}/>} />
      <Route exact path="/Cart" render = {(props) => <Cart {...props} products={product}/>}/>

      <Route exact path="/stripe" render={(props) => <Stripe {...props} products={product}/>} />
      <Route exact path="/payment/success" render={(props) => <PaymentSuccess {...props} />}/>
      <Route exact path="/payment/error" render={(props) => <PaymentError {...props} />}/>
      <Footer/>  
  
     </Router>
  );
}

export default App;
