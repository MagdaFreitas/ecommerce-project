import React from "react";
import { NavLink } from "react-router-dom";
import star_favourite from './star_favourite.png'
import avatar from './avatar.png'
import storage_database from './storage_database.png'
import cart_market from './cart_market.png'


const Navbar = (props) => {
  return (
    <div style={styles.navStyles} className='nav'>
     
<NavLink exact to={"/"}> <img  className='icon' src={star_favourite}/> </NavLink>
<NavLink exact to={"/All"} style={styles.default} className='nav'>All products</NavLink>

      <span style={styles.default}>
  <div className="dropdown">
    <span className="dropbtn" >Categories</span>
    <div className="dropdown-content">
      <NavLink exact to={"/Categories/Tshirts"}>T-shirts</NavLink>
      <NavLink exact to={"/Categories/Games"}>Games</NavLink>
      <NavLink exact to={"/Categories/Homeware"}>Homeware</NavLink>
    </div>
  </div> 
      </span>

<NavLink exact to={"/BestSellers"} style={styles.default}>Best Sellers</NavLink>
<NavLink exact to={"/OnSale"} style={styles.default}>On Sale</NavLink>
<NavLink exact to={"/Cart"}> <img  className='icon' src={cart_market}/></NavLink>

{props.isAdmin === true ? (<NavLink exact to={"/users/secret-page"} style={styles.default}>
  <img  className='icon' src={storage_database}/></NavLink> ) : null}

{props.isLoggedIn === false ? (<NavLink exact style={styles.default} className='log' to={"/users"}>Login</NavLink>) : null}

{props.isLoggedIn === true ? (<NavLink exact style={styles.default} className='log' to={"/users/userPage"}><img  className='icon' src={avatar}/></NavLink>) : null}
    </div>
  );
};

export default Navbar;

const styles = {
  navStyles: {
    display: "flex",
    height: 60,
    alignItems: "center",
    justifyContent: "space-around",
    color: "#518ab5",
    backgroundColor:'#ffea94'
  },
}

