import React from 'react';

let Footer = () => {
  return(
    <footer className='footer'>

  <h2>Find out more!</h2>

 <p><a href="https://www.facebook.com/magda.a.morais/">Facebook</a></p>
 <p><a href="https://www.instagram.com/magdaamorais/">Instagram</a></p>
 <p><a href="https://twitter.com/mmsf03">Twitter</a></p>
 <p><a href="https://www.linkedin.com/in/magda-m-a39016122/">Linkedin</a></p>
 <p>Not ™, ℠, ® nor ©. Barcelona 2020.</p>
  
  </footer>
  )
  }

export default Footer;