import React from "react";

const Product = (props) => {
  return (
    <div className="product_container">
      <div className="image_container">
        <img alt='productPic' className='checkOutImg' src={props.itemProduct.images}/>
      </div>
      <div className="text_container">
        <p>product : {props.itemProduct.name}</p>
      </div>
      <div className="text_container">
        <p>quantity : {props.itemProduct.quantity}</p>
      </div>
      <div className="text_container">
        <p>price : {props.itemProduct.amount} €</p>
      </div>
      <div className="text_container">
        <p>total price : {props.itemProduct.amount * props.itemProduct.quantity} €</p>
      </div>
    </div>
  );
};

export default Product;
