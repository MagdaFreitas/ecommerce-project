import React from 'react'
import axios from 'axios'
import widgetStyle from './widgetStyle';

const UploadImages = props => {

	const uploadWidget = () => {
        window.cloudinary.openUploadWidget({ 
        	cloud_name: process.env.REACT_APP_CLOUD_NAME, 
        	upload_preset: process.env.REACT_APP_UPLOAD_PRESET, 
			tags:['user'],
			stylesheet:widgetStyle
        },(error, result)=> {
				    debugger
                if(error){
					debugger
                }else{
					props.setImageUpload({photo_url:result[0].secure_url, public_id:result[0].public_id})			  
                }
            });
	}
		return (
			<div className="flex_upload">
                <div className="upload">
					<button className ="button"
                    	onClick={uploadWidget} > Upload
                    </button>
                </div>
            </div>
		)
}

export default UploadImages
